//constants
const fs = require('fs');
const path = require("path");
const filesPath = path.resolve('./files');
const availableExt = ['txt', 'json', 'xml', 'js', 'log', 'yaml'];
let filesArr = []
const fsp = require('fs/promises');
//functions
//createFile
function createFile (req, res, next) {
  try {
    let data = req.body
    //check if filename is undefined or empty
    if (data.filename === undefined && data.filename === null) {
      res.status(400).json({message: 'Write filename'});
      return;
    }
    //check if content is undefined 
    if (data.content === undefined) {
      res.status(400).json({message: 'Write some content'});
      return;
    }
    let extension = data.filename.split('.').pop()
    //check ext
    if (availableExt.includes(extension)) {
      fs.writeFileSync(path.join(filesPath, data.filename), data.content, { flag: 'a+' });
      res.status(200).send({ "message": "File created successfully" });
    } else {
      res.status(400).send({
        message: `File extension is inappropriate`
      });
    }
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
    
}
//getFiles
function getFiles (req, res, next) {
  fs.readdir(filesPath, (err, data) => {
    if (err) {
      console.log(err);
      return;
    };
    data.forEach(file => {
      filesArr.push(file);
      return filesArr;
    })
    res.status(200).send({
      "message": "Success",
      "files": [...new Set(filesArr)]});
  }) 
}
//getFile
const getFile = async (req, res, next) => {
  try {
    let newDate;
    let extension = path.extname(req.url).split('.').pop();
    fs.stat(filesPath + req.url, (error, stats) => {
      if (error) {
        console.log(error);
        return;
      }
      newDate = stats.birthtime;
    });
    res.status(200).send({
      "message": "Success",
      "filename": path.basename(req.url),
      "content": await fsp.readFile(filesPath + req.url, { encoding: 'utf8' }),
      "extension": extension,
      "uploadedDate": newDate});
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}

//editFile
const editFile = (req, res, next) => {
  try {
    let data = req.body
    fs.writeFile(path.join(filesPath, data.filename), data.content, 'utf-8', err => {
      if (error) {
        console.log(error);
        return;
      }
      res.status(200).send({
        "message": "File was edited successfully",
      })
  });
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}
//deleteFile
const deleteFile = (req, res, next) => {
  try {
    let data = req.body
    fs.unlink(path.join(filesPath, data.filename), err => {
      if (err) throw err;
    })
    res.status(200).send({
      "message": "File was deleted successfully",
    });
  } catch (error) {
    res.status(400).json({message: 'Server error'});
  }
}
//exports
module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
